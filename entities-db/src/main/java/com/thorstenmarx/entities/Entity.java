/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thorstenmarx.entities;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author marx
 */
public class Entity extends Identifier {
	
	private String name;
	private final String type;
	
	private final Map<String, Attribute> attributes = new HashMap<>();
	
	public Entity (final String type) {
		this.type = type;
	}

	public Map<String, Attribute> attributes () {
		return attributes;
	}
	public void addAttribute (final Attribute attribute) {
		attributes.put(attribute.name(), attribute);
	}
	
	public String name() {
		return name;
	}

	public Entity name(String name) {
		this.name = name;
		return this;
	}

	public String type() {
		return type;
	}
	
	
}
