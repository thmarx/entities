package com.thorstenmarx.entities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.h2.jdbcx.JdbcConnectionPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author marx
 */
public class Entities {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Entities.class);

	final String path;

	private JdbcConnectionPool pool;

	private Entities(final String path) {
		this.path = path;
	}

	public void open() {
		try {
			Class.forName("org.h2.Driver");
			pool = JdbcConnectionPool.create("jdbc:h2:" + path, "sa", "sa");
			init();
		} catch (ClassNotFoundException ex) {
			LOGGER.error("", ex);
			throw new RuntimeException(ex);
		}
	}

	private void init() {
		try (Connection connection = pool.getConnection()) {
			Statement st = connection.createStatement();
			st.execute("CREATE TABLE IF NOT EXISTS entities (id VARCHAR(255) UNIQUE, name VARCHAR(255), type VARCHAR(255))");
			st.execute("CREATE TABLE IF NOT EXISTS attributes (id VARCHAR(255) UNIQUE, entity_id VARCHAR(255), name VARCHAR(255), type VARCHAR(255), value VARCHAR, FOREIGN KEY(entity_id) REFERENCES entities(id))");

			st.execute("CREATE INDEX IF NOT EXISTS ENTITY_ID ON entities(id)");
			st.execute("CREATE INDEX IF NOT EXISTS ATTRIBUTE_ID ON attributes(id)");
			st.execute("CREATE INDEX IF NOT EXISTS ATTRIBUTE_ENTITY_ID ON attributes(entity_id)");

			
			connection.commit();
			st.close();
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	public Result list(final String type, final int offset, final int limit) {
		try (Connection connection = pool.getConnection()) {

			String statement = "SELECT * FROM entities WHERE type = ? LIMIT ? OFFSET ?";

			int count = count(type);
			if (count == 0) {
				return Result.EMPTY;
			}
			Result result = new Result(count, offset, limit);

			try (PreparedStatement ps = connection.prepareStatement(statement)) {
				ps.setString(1, type);
				ps.setInt(2, limit);
				ps.setInt(3, offset);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					Entity entity = createEntity(rs);
					result.add(entity);
				}
			}
			return result;
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	public Entity get(final String id) {
		try (Connection connection = pool.getConnection()) {

			String statement = "SELECT * FROM entities WHERE id = ?";

			try (PreparedStatement ps = connection.prepareStatement(statement)) {
				ps.setString(1, id);
				ResultSet rs = ps.executeQuery();
				if (rs.next()) {
					Entity entity = createEntity(rs);
					
					loadAttributes(entity, connection);
					
					return entity;
				}
			}
			return null;
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	private Entity createEntity(final ResultSet rs) throws SQLException {
		final String name = rs.getString("name");
		final String type = rs.getString("type");
		final String id = rs.getString("id");
		final Entity entity = new Entity(type);
		entity.id(id);
		entity.name(name);
		return entity;
	}

	private Attribute createAttribute(final ResultSet rs) throws SQLException {
		final String name = rs.getString("name");
		final String type = rs.getString("type");
		final String value = rs.getString("value");
		final String id = rs.getString("id");

		if (!Attribute.TYPE.is(type)) {

		}
		Attribute attribute = Attribute.create(name, Attribute.TYPE.valueOf(type));
		attribute.id(id);

		switch (attribute.type()) {
			case STRING:
				attribute.value(value);
				break;
			case DOUBLE:
				attribute.value(Double.valueOf(value));
				break;
			case FLOAT:
				attribute.value(Float.valueOf(value));
				break;
			case INTEGER:
				attribute.value(Integer.valueOf(value));
				break;
			case LONG:
				attribute.value(Long.valueOf(value));
				break;
			case BOOLEAN:
				attribute.value(Boolean.valueOf(value));
				break;
			default:
				attribute.value(value);
		}

		return attribute;
	}

	public int count(final String type) {
		try (Connection connection = pool.getConnection()) {

			String statement = "SELECT count(id) as count FROM entities WHERE type = ?";

			try (PreparedStatement ps = connection.prepareStatement(statement)) {
				ps.setString(1, type);
				ResultSet rs = ps.executeQuery();
				if (rs.next()) {
					return rs.getInt("count");
				}
			}
			return 0;
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
	}

	public boolean add(final Entity entity) {

		boolean create = false;
		if (entity.id() == null) {
			create = true;
			entity.id(UUID.randomUUID().toString());
		}

		try (Connection connection = pool.getConnection()) {

			String statement;
			if (create) {
				statement = "INSERT INTO entities (id, name, type) VALUES(?, ?, ?)";
			} else {
				statement = "UPDATE entities SET name = ? WHERE id=?";
			}

			try (PreparedStatement ps = connection.prepareStatement(statement)) {
				if (create) {
					ps.setString(1, entity.id());
					ps.setString(2, entity.name());
					ps.setString(3, entity.type());
				} else {
					ps.setString(1, entity.name());
					ps.setString(2, entity.id());
				}
				ps.execute();
				
				addAttributes(entity, connection);
				
				connection.commit();
			}
		} catch (SQLException ex) {
			throw new RuntimeException(ex);

		}

		return false;
	}
	
	/**
	 * Delete an entity and all attributes;
	 * 
	 * @param id 
	 */
	public void delete (final String id) {
		try (Connection connection = pool.getConnection()) {

			PreparedStatement st = connection.prepareStatement("DELETE FROM attributes WHERE entity_ID = ?");
			st.setString(1, id);
			st.execute();
			st = connection.prepareStatement("DELETE FROM entities WHERE id = ?");
			st.setString(1, id);
			st.execute();

			connection.commit();
			st.close();
		} catch (SQLException ex) {
			throw new RuntimeException(ex);

		}
	}
	/**
	 * Delete an attribute;
	 * 
	 * @param id 
	 */
	public void deleteAttribute (final String id) {
		try (Connection connection = pool.getConnection(); PreparedStatement st = connection.prepareStatement("DELETE FROM attributes WHERE id = ?")) {

			st.setString(1, id);
			st.execute();

			connection.commit();
		} catch (SQLException ex) {
			throw new RuntimeException(ex);

		}
	}
	
	private void addAttributes (final Entity entity, final Connection connection) throws SQLException {

		PreparedStatement UPDATE = null;
		try (PreparedStatement INSERT = connection.prepareStatement("INSERT INTO attributes (id, entity_id, name, type, value) VALUES (?, ?, ?, ?, ?)")) {
			UPDATE = connection.prepareStatement("UPDATE attributes set value = ? WHERE id = ?");
			for (Attribute attribute : entity.attributes().values()) {
				boolean create = false;
				if (attribute.id() == null) {
					create = true;
					attribute.id(UUID.randomUUID().toString());
				}
				if (create) {
					INSERT.setString(1, attribute.id());
					INSERT.setString(2, entity.id());
					INSERT.setString(3, attribute.name());
					INSERT.setString(4, attribute.type().name());
					INSERT.setString(5, String.valueOf(attribute.value()));
					
					INSERT.execute();
				} else {
					UPDATE.setString(1, String.valueOf(attribute.value()));
					UPDATE.setString(2, attribute.id());
				}
			}
		} finally {
			if (UPDATE != null) {
				UPDATE.close();
			}
		}
		

	}

	/**
	 * Close the entities instance and shutdown the database connection.
	 */
	public void close() {
		pool.dispose();
	}

	public static Entities create(final String path) {
		return new Entities(path);
	}

	private void loadAttributes(Entity entity, Connection connection) throws SQLException {
		try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM attributes WHERE entity_ID = ?")) {
			ps.setString(1, entity.id());
			
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Attribute attr = createAttribute(rs);
				entity.attributes().put(attr.name(), attr);
			}
		}
	}

	

	public static class Result extends ArrayList<Entity> {

		public static final Result EMPTY = new Result(0, 0, 0);
		private static final long serialVersionUID = 8605787757855134693L;

		private final int totalSize;
		private final int offset;
		private final int limit;

		public Result(final int totalSize, final int offset, final int limit) {
			super();
			this.totalSize = totalSize;
			this.offset = offset;
			this.limit = limit;
		}

		public int totalSize() {
			return totalSize;
		}

		public int offset() {
			return offset;
		}

		public int limit() {
			return limit;
		}

	}
}
