/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thorstenmarx.entities;

/**
 * SQL:
 * CREATE TABLE IF NOT EXISTS attributes (id VARCHAR(255) UNIQUE, name VARCHAR(255), value VARCHAR())
 *
 * @author marx
 */
public class Attribute extends Identifier {
	public enum TYPE {
		INTEGER, FLOAT, DOUBLE, STRING, BOOLEAN, LONG;
		
		public static boolean is (final String type) {
			for (TYPE aType : values()) {
				if (aType.name().equals(type)) {
					return true;
				}
			}
					
			return false;
		}
	}
	private final String name;
	
	private final TYPE type;
	
	private Object value;
	
	Attribute (final String name, final TYPE type) {
		this.type = type;
		this.name = name;
	}

	public String name() {
		return name;
	}

	public TYPE type() {
		return type;
	}

	public Object value() {
		return value;
	}

	public Attribute value(Object value) {
		this.value = value;
		return this;
	}
	
	public <T> T value (Class<T> valueType) {
		if (valueType.isInstance(value)) {
			return valueType.cast(value);
		}
		return null;
	}
	
	public static Attribute create (final String name, final TYPE type) {
		return new Attribute(name, type);
	}
	
}
