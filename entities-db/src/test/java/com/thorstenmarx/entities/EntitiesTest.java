/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thorstenmarx.entities;

import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;


/**
 *
 * @author marx
 */
public class EntitiesTest {
	
	private static final String TYPE = "atype";
	private static final String OTHER_TYPE = "otype";
	private static final String EMPTY_TYPE = "etype";
	
	private static final String NAME = "test name";
	
	Entities entities;

	String id;
	
	@Test
	public void testOpen() {
		entities = Entities.create("./build/testdb" + System.currentTimeMillis());
		entities.open();
	}
	
	@Test(dependsOnMethods = {"testOpen"})
	public void testAddEntity () {
		Entity entity = new Entity(TYPE);
		entity.name(NAME);
		entities.add(entity);
		id = entity.id();
		Assertions.assertThat(entity.id()).isNotNull().isNotEmpty();
	}
	@Test(dependsOnMethods = {"testAddEntity"})
	public void testGet () {
		Entity entity = entities.get(id);
		Assertions.assertThat(entity).isNotNull();
		Assertions.assertThat(entity.name()).isEqualTo(NAME);
	}
	@Test(dependsOnMethods = {"testAddEntity"})
	public void testCount () {
		for (int i = 0; i < 10 ; i++) {
			Entity entity = new Entity(OTHER_TYPE);
			entities.add(entity);
		}
		int count = entities.count(EMPTY_TYPE);
		Assertions.assertThat(count).isZero();
		count = entities.count(OTHER_TYPE);
		Assertions.assertThat(count).isEqualTo(10);
	}
	@Test(dependsOnMethods = {"testCount"})
	public void testList () {
		Entities.Result result = entities.list(OTHER_TYPE, 0, 5);
		
		Assertions.assertThat(result).hasSize(5);
		Assertions.assertThat(result.totalSize()).isEqualTo(10);
		
		result = entities.list(OTHER_TYPE, 5, 5);
		
		Assertions.assertThat(result).hasSize(5);
		Assertions.assertThat(result.totalSize()).isEqualTo(10);
	}
	@Test(dependsOnMethods = {"testCount"})
	public void testAttributes () {
		
		Entity entity = new Entity(TYPE);
		entity.addAttribute(Attribute.create("aFloat", Attribute.TYPE.FLOAT).value(new Float(1)));
		entity.addAttribute(Attribute.create("aInteger", Attribute.TYPE.INTEGER).value(new Integer(2)));
		entity.addAttribute(Attribute.create("aBoolean", Attribute.TYPE.BOOLEAN).value(Boolean.TRUE));
		entity.addAttribute(Attribute.create("aDouble", Attribute.TYPE.DOUBLE).value(new Double(3)));
		entity.addAttribute(Attribute.create("aLong", Attribute.TYPE.LONG).value(new Long(4)));
		entity.addAttribute(Attribute.create("aString", Attribute.TYPE.STRING).value(new String("hallo")));
		entities.add(entity);
		
		Entity e = entities.get(entity.id());
		
		Assertions.assertThat(e.attributes()).hasSize(6);
		Assertions.assertThat(e.attributes().get("aFloat")).isNotNull();
		Assertions.assertThat(e.attributes().get("aFloat").type()).isEqualByComparingTo(Attribute.TYPE.FLOAT);
		Assertions.assertThat(e.attributes().get("aFloat").value(Float.class)).isEqualTo(1f);
		
		Assertions.assertThat(e.attributes().get("aInteger")).isNotNull();
		Assertions.assertThat(e.attributes().get("aInteger").type()).isEqualByComparingTo(Attribute.TYPE.INTEGER);
		Assertions.assertThat(e.attributes().get("aInteger").value(Integer.class)).isEqualTo(2);
		
		Assertions.assertThat(e.attributes().get("aBoolean")).isNotNull();
		Assertions.assertThat(e.attributes().get("aBoolean").type()).isEqualByComparingTo(Attribute.TYPE.BOOLEAN);
		Assertions.assertThat(e.attributes().get("aBoolean").value(Boolean.class)).isEqualTo(Boolean.TRUE);
		
		Assertions.assertThat(e.attributes().get("aDouble")).isNotNull();
		Assertions.assertThat(e.attributes().get("aDouble").type()).isEqualByComparingTo(Attribute.TYPE.DOUBLE);
		Assertions.assertThat(e.attributes().get("aDouble").value(Double.class)).isEqualTo(3);
		
		Assertions.assertThat(e.attributes().get("aLong")).isNotNull();
		Assertions.assertThat(e.attributes().get("aLong").type()).isEqualByComparingTo(Attribute.TYPE.LONG);
		Assertions.assertThat(e.attributes().get("aLong").value(Long.class)).isEqualTo(4);
		
		Assertions.assertThat(e.attributes().get("aString")).isNotNull();
		Assertions.assertThat(e.attributes().get("aString").type()).isEqualByComparingTo(Attribute.TYPE.STRING);
		Assertions.assertThat(e.attributes().get("aString").value(String.class)).isEqualTo("hallo");
		
	}
	@Test(dependsOnMethods = {"testCount"})
	public void testDeleteAttribute () {
		
		Entity entity = new Entity(TYPE);
		Attribute attr = Attribute.create("aFloat", Attribute.TYPE.FLOAT).value(new Float(1));
		entity.addAttribute(attr);
		entity.addAttribute(Attribute.create("aInteger", Attribute.TYPE.INTEGER).value(new Integer(2)));
		entities.add(entity);
		
		entities.deleteAttribute(attr.id());
		
		Entity e = entities.get(entity.id());
		Assertions.assertThat(e.attributes()).hasSize(1);
		
	}
	@Test(dependsOnMethods = {"testCount"})
	public void testDeleteEntity () {
		
		Entity entity = new Entity(TYPE);
		Attribute attr = Attribute.create("aFloat", Attribute.TYPE.FLOAT).value(new Float(1));
		entity.addAttribute(attr);
		entity.addAttribute(Attribute.create("aInteger", Attribute.TYPE.INTEGER).value(new Integer(2)));
		entities.add(entity);
		
		entities.delete(entity.id());
		
		Entity e = entities.get(entity.id());
		Assertions.assertThat(e).isNull();
		
	}
	@Test(dependsOnMethods = {"testList"})
	public void testClose () {
		entities.close();
	}
}
